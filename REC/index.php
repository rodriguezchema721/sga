<?php 
    include("db.php")
?>

<?php 
    include("header.php")
?>

<nav class="navbar navbar-expand-sm bg-success navbar-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">
      <img src="img5.png" alt="Logo" style="width:40px;" class="rounded-pill">
      RECLUTAMIENTO
    </a>
    <ul class="navbar-nav">
      <li class="nav-item">
        <button class="nav-link active" onclick="document.getElementById('id01').style.display='block'" >
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/></svg>
        Buscar</button>
      </li>
    </ul>
    <div class="d-grid gap-2">
        <button onclick="location.href='/sga/cate.html'" type="submit" class="btn btn-danger" >
            Menu Principal
        </button>
    </div>
  </div>
</nav>

<div id="id01" class="modal">
  <form class="modal-contents animate" action="log.php" method="post">
    <div class="imgcontainer">
        <h1>BUSCADOR RECLUTAMIENTO</h1>
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <!-- buscador basico -->
<div class="center mt-2">
        <div class="card pt-3" >
                <div class="container-fluid p-2 ">
                        <div class=" col-12 mt-2">
                                <div class="table-responsive">
                                        <div class="mb-3"> 
                                                <label class="form-label"><b>Ingrese Elemento a Buscar</b></label>
                                                <input onkeyup="buscar_ahora($('#buscar_1').val());" type="text" class="form-control" id="buscar_1" name="buscar_1">
                                        </div>
                                        <div class="card col-12 mt-5">
                                                <div class="card-body">
                                                        <div id="datos_buscador" class="container pl-5 pr-5" style="border: none;"></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<!-- END buscador basico -->

<script type="text/javascript">
        function buscar_ahora(buscar) {
        var parametros = {"buscar":buscar};
        $.ajax({
        data:parametros,
        type: 'POST',
        url: 'buscador.php',
        success: function(data) {
        document.getElementById("datos_buscador").innerHTML = data;
        }
        });
        }
      
</script>

  </form>
</div>

<div class="container p-4">
    <div class="row">
        <!--Aqui empieza el formulario-->
        <div class="col-md-3" style=" position:absolute; top:70px; left:0px;">
            <?php
            if (isset($_SESSION['message'])) {?>
                <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message']?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php session_unset(); } ?>

            <div class="card card-body">
                <form action="guardar.php" method="post">
                    <div class="form-group">
                    <label for="floatingInputValue"><b>Matricula</label>
                        <input type="text" name="matricula" class="form-control" placeholder="Ingrese la Matricula" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Ingrese el Nombre" autofocus>
                    </div>
                    
                    <div class="form-group">
                    <label for="floatingInputValue">Clase</label>
                        <input type="text" name="clase" class="form-control" placeholder="Ingrese la Clase" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Año 18's</label>
                        <input type="text" name="anio" class="form-control" placeholder="Ingrese el Anio" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Remiso</label>
                        <input type="text" name="remiso" class="form-control" placeholder="SI / NO" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Volumen</label>
                        <input type="text" name="volumen" class="form-control" placeholder="Ingrese el Volumen" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Expediente</label>
                        <input type="text" name="expediente" class="form-control" placeholder="Ingrese el Expediente" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Hoja o Folio</label>
                        <input type="text" name="hoja" class="form-control" placeholder="Ingrese la Hoja o Folio" autofocus>
                    </div>
                    <div class= "d-grid gap-2">
                        <input class="btn btn-success" type="submit" name="save" style="margin-top:5px;"></input>
                    </div>
                </form>
            </div>
        </div>
        <!--Aqui termina el formulario-->

        <div class="col-md-9 offset-md-3" style=" position:absolute; top:70px; left:00px;" id="id01">
                <table class="table table-bordered">
                    <thead> 
                    <tr>
                        <th>Matricula</th>
                        <th>Nombre</th> 
                        <th>Clase</th>
                        <th>Año 18</th>
                        <th>Remiso</th>
                        <th>Volumen</th>
                        <th>Expediente</th>
                        <th>Hoja o Folio</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                        <tbody>
                          <?php
                            $query = "SELECT * FROM reclutamiento";
                            $result_r = mysqli_query($conn,$query);

                            while ($row=mysqli_fetch_array($result_r)) {?>
                                <tr id="myUL">
                                  <td><?php echo $row['matricula']?></td>
                                  <td><?php echo $row['nombre']?></td> 
                                  <td><?php echo $row['clase']?></td>
                                  <td><?php echo $row['anio']?></td>
                                  <td><?php echo $row['remiso']?></td>
                                  <td><?php echo $row['volumen']?></td>
                                  <td><?php echo $row['expediente']?></td>
                                  <td><?php echo $row['folio']?></td>
                                  <td>
                                      <a href="editar.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
                                          <i class = "fas fa-marker"></i>
                                      </a>
                                      <a href="borrar.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                                          <i class="far fa-trash-alt"></i>
                                      </a>
                                  </td>
                                </tr>
                            <?php }?>

                        </tbody>
                </table> 
            </div>
    </div>
</div>

<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<?php 
    include("footer.php")
?>