-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-07-2022 a las 06:25:31
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sga`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apdef`
--

CREATE TABLE `apdef` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `notas` text NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apdiv`
--

CREATE TABLE `apdiv` (
  `id` int(11) NOT NULL,
  `hombre` int(11) NOT NULL,
  `mujer` int(11) NOT NULL,
  `notas` int(11) NOT NULL,
  `volumen` int(11) NOT NULL,
  `expediente` int(11) NOT NULL,
  `folio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `apdiv`
--

INSERT INTO `apdiv` (`id`, `hombre`, `mujer`, `notas`, `volumen`, `expediente`, `folio`) VALUES
(0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apmat`
--

CREATE TABLE `apmat` (
  `id` int(11) NOT NULL,
  `hombre` varchar(200) NOT NULL,
  `mujer` varchar(200) NOT NULL,
  `notas` text NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apnac`
--

CREATE TABLE `apnac` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `notas` text NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprec`
--

CREATE TABLE `aprec` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `notas` text NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catastro`
--

CREATE TABLE `catastro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `clave` varchar(200) NOT NULL,
  `domicilio` varchar(200) NOT NULL,
  `cuenta` varchar(200) NOT NULL,
  `anterior` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `terreno` varchar(200) NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `catastro`
--

INSERT INTO `catastro` (`id`, `nombre`, `clave`, `domicilio`, `cuenta`, `anterior`, `fecha`, `lugar`, `terreno`, `volumen`, `expediente`, `folio`) VALUES
(1, 'PEREZ LOPEZ JUAN', '123456789987654321', 'NICOLAS NAVARRO SN NUMERO', '123456', '123456789987654321', '2022-05-04', 'CALIMAYA', 'EL MALO', '1', '1', '1'),
(3, 'A', 'A', 'A', 'A', '', '0000-00-00', 'A', 'A', 'A', 'A', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrayentes`
--

CREATE TABLE `contrayentes` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hombre` varchar(200) NOT NULL,
  `mujer` varchar(200) NOT NULL,
  `residencia` varchar(200) NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `defunciones`
--

CREATE TABLE `defunciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `edad` varchar(200) NOT NULL,
  `muerte` varchar(200) NOT NULL,
  `residencia` varchar(200) NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacimientos`
--

CREATE TABLE `nacimientos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `padre` varchar(200) NOT NULL,
  `madre` varchar(200) NOT NULL,
  `residencia` varchar(200) NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nacimientos`
--

INSERT INTO `nacimientos` (`id`, `nombre`, `fecha`, `padre`, `madre`, `residencia`, `volumen`, `expediente`, `folio`) VALUES
(4, 'JOSE MANUEL RODRIGUEZ VELAZQUEZ', '2001-11-27', 'RENE RODRIGUEZ REYES', 'MARTINA VELAZQUEZ SANCHEZ', 'ZARAGOZA', '14', '12', '25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclutamiento`
--

CREATE TABLE `reclutamiento` (
  `id` bigint(100) NOT NULL,
  `matricula` varchar(200) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `clase` varchar(200) NOT NULL,
  `anio` varchar(200) NOT NULL,
  `remiso` varchar(200) NOT NULL,
  `volumen` varchar(200) NOT NULL,
  `expediente` varchar(200) NOT NULL,
  `folio` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reclutamiento`
--

INSERT INTO `reclutamiento` (`id`, `matricula`, `nombre`, `clase`, `anio`, `remiso`, `volumen`, `expediente`, `folio`) VALUES
(37, '201923075', 'JULIO CESAR HUERTAS NAJERA', '2001', '2019', 'SI', '14', '2', '89'),
(38, '201923085', 'ANABEL VARGAS ITURBIDE', '2001', '2019', 'SI', '3', '10', '80'),
(39, '201434300249', 'JUAN CARLOS LOPEZ SERRANO', '1998', '2017', 'NO', '14', '12', '87');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `apdef`
--
ALTER TABLE `apdef`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `apmat`
--
ALTER TABLE `apmat`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `apnac`
--
ALTER TABLE `apnac`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `aprec`
--
ALTER TABLE `aprec`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catastro`
--
ALTER TABLE `catastro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contrayentes`
--
ALTER TABLE `contrayentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `defunciones`
--
ALTER TABLE `defunciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nacimientos`
--
ALTER TABLE `nacimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reclutamiento`
--
ALTER TABLE `reclutamiento`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `apdef`
--
ALTER TABLE `apdef`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `apmat`
--
ALTER TABLE `apmat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `apnac`
--
ALTER TABLE `apnac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `aprec`
--
ALTER TABLE `aprec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `catastro`
--
ALTER TABLE `catastro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contrayentes`
--
ALTER TABLE `contrayentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `defunciones`
--
ALTER TABLE `defunciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `nacimientos`
--
ALTER TABLE `nacimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `reclutamiento`
--
ALTER TABLE `reclutamiento`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
