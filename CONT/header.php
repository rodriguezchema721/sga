<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>CONTRAYENTES</title>
    <link rel="icon" href="img6.png">
    <link rel="icon" href="../Images/img6.png">
    <style>
      body {font-family: Arial, Helvetica, sans-serif;}

.cuerpos{
    background-image: url('img1.png');
}

h1{
  margin-top: 20px;
  height: 40px;
  font-family: 'Segoe UI';
  font-weight: bold;
  font-size: 50px;
  -webkit-text-stroke: 2px black;
  color: rgb(255, 255, 255);
  margin: 1em auto 1em;
  float: center;
  text-align: center;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */
button {
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  font-size: 18px;
  font-family:Arial;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: 100%;
  padding: 10px 18px;
  background-color: #f44336;
}

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  .form-login {
    width: 300px;
    height: 340px;
    background: #4e4d4d;
    margin: auto;
    margin-top: 10px;
    box-shadow: 7px 13px 37px #000;
    padding: 20px 30px;
    border-top: 4px solid #017bab;
    color: white;
  }

  .botones {
    width: 100%;
    height: 40px;
    background: #017bab;
    border: none;
    color: white;
    margin-bottom: 16px;
  }

  .form-login h2 {
    margin: 0;
    text-align: center;
    height: 40px;
    margin-bottom: 20px;
    border-bottom: 1px solid;
    font-size: 20px;
  }
/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 10px 0 5px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 0%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 60%; /* Full width */
  height: 60%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  padding-top: 10px;
}

/* Modal Content/Box */
.modal-contents {
  /*background-color:blu;*/
  margin: 1% auto 1% auto; /* 5% from the top, 15% from the bottom and centered */
  border: 2px solid rgb(87, 158, 47);
  background-image: url('../Images/img6.png');
  width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}

/* Add Zoom Animation */
.animate {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 75%;
  }
}
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <!-- BOOTSTRAP 4 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  </head>
  <body>