<?php
include("db.php");

if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM catastro WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $n = $row['nombre'];
    $cl= $row['clave'];
    $d = $row['domicilio'];
    $cu= $row['cuenta'];
    $a = $row['anterior'];
    $f = $row['fecha'];
    $l = $row['lugar'];
    $t = $row['terreno'];
    $v = $row['volumen'];
    $e = $row['expediente'];
    $h = $row['folio'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $n = $_POST['nombre'];
  $cl = $_POST['clave'];
  $d = $_POST['domicilio'];
  $cu = $_POST['cuenta'];
  $a = $_POST['anterior'];
  $f = $_POST['fecha'];
  $l = $_POST['lugar'];
  $t = $_POST['terreno'];
  $v = $_POST['volumen'];
  $e = $_POST['expediente'];
  $h = $_POST['hoja'];

  $query = "UPDATE catastro set nombre = '$n', clave = '$cl', domicilio = '$d', cuenta = '$cu', anterior = '$r', fecha = '$f', lugar = '$l', terreno = '$t', volumen = '$v', expediente = '$e', folio = '$h' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Modificado Satisfactoriamente';
  $_SESSION['message_type'] = 'warning';
  header('location: cat.php');
}

?>
<?php include('header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="editar.php?id=<?php echo $_GET['id']; ?>" method="POST">
                    <div class="form-group">
                    <label for="floatingInputValue"><b>Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Inrese el Nombre" autofocus value="<?php echo $row['nombre']?>">
                    </div>
                    
                    <div class="form-group">
                    <label for="floatingInputValue">Clave Catastral</label>
                        <input type="text" name="clave" class="form-control" placeholder="Inrese la Clave" autofocus value="<?php echo $row['clave']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Domicilio</label>
                        <input type="text" name="domicilio" class="form-control" placeholder="Inrese el Domicilio" autofocus value="<?php echo $row['domicilio']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">N° Cuenta Prediial</label>
                        <input type="text" name="cuenta" class="form-control" placeholder="Inrese la Cuenta" autofocus value="<?php echo $row['cuenta']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Clave Anterior</label>
                        <input type="text" name="anterior" class="form-control" placeholder="Inrese la Clave" autofocus value="<?php echo $row['anterior']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Fecha de Traslado</label>
                        <input type="date" name="fecha" class="form-control" placeholder="Inrese la fecha" autofocus value="<?php echo $row['fecha']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Lugar</label>
                        <input type="text" name="lugar" class="form-control" placeholder="Inrese la Residencia" autofocus value="<?php echo $row['lugar']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Nombre del Terreno</label>
                        <input type="text" name="terreno" class="form-control" placeholder="Inrese el Nombre" autofocus value="<?php echo $row['terreno']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Volumen</label>
                        <input type="text" name="volumen" class="form-control" placeholder="Inrese el Volumen" autofocus value="<?php echo $row['volumen']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Expediente</label>
                        <input type="text" name="expediente" class="form-control" placeholder="Inrese el Expediente" autofocus value="<?php echo $row['expediente']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Hoja o Folio</label>
                        <input type="text" name="hoja" class="form-control" placeholder="Inrese la Hoja o Folio" autofocus value="<?php echo $row['folio']?>">
                    </div>
                    <div class= "d-grid gap-2">
                        <input class="btn btn-success" type="submit" name="update" style="margin-top:5px;"></input>
                    </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>