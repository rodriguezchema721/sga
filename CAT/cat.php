<?php 
    include("db.php");
?>

<?php 
    include("header.php");
?>

<nav class="navbar navbar-expand-sm bg-danger navbar-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">
      <img src="../Images/img7.png" alt="Logo" style="width:40px;" class="rounded-pill">
      CATASTRO
    </a>
    <ul class="navbar-nav">
      <li class="nav-item">
        <button class="nav-link active" onclick="document.getElementById('id01').style.display='block'" >
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/></svg>
        Buscar</button>
      </li>
    </ul>
    <div class="d-grid gap-2">
        <button onclick="location.href='/sga/cate.html'" type="submit" class="btn btn-primary" >
            Menu Principal
        </button>
    </div>
  </div>
</nav>

<div id="id01" class="modal">
  <form class="modal-contents animate" action="log.php" method="post">
    <div class="imgcontainer">
        <h1>BUSCADOR CATASTRO</h1>
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <!-- buscador basico -->
<div class="center mt-2">
        <div class="card pt-3" >
                <div class="container-fluid p-2 ">
                        <div class=" col-12 mt-2">
                                <div class="table-responsive">
                                        <div class="mb-3"> 
                                                <label class="form-label"><b>Ingrese Elemento a Buscar</b></label>
                                                <input onkeyup="buscar_ahora($('#buscar_1').val());" type="text" class="form-control" id="buscar_1" name="buscar_1">
                                        </div>
                                        <div class="card col-12 mt-5">
                                                <div class="card-body">
                                                        <div id="datos_buscador" class="container pl-5 pr-5" style="border: none;"></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<!-- END buscador basico -->

<script type="text/javascript">
        function buscar_ahora(buscar) {
        var parametros = {"buscar":buscar};
        $.ajax({
        data:parametros,
        type: 'POST',
        url: 'buscador.php',
        success: function(data) {
        document.getElementById("datos_buscador").innerHTML = data;
        }
        });
        }
      
</script>

  </form>
</div>

<div class="container p-4">
    <div class="row">
        <!--Aqui empieza el formulario-->
        <div class="col-md-3" style=" position:absolute; top:70px; left:0px;">
            <?php
            if (isset($_SESSION['message'])) {?>
                <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                <?= $_SESSION['message']?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php session_unset(); } ?>

            <div class="card card-body">
                <form action="guardar.php" method="post">
                    <div class="form-group">
                    <label for="floatingInputValue"><b>Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Inrese el Nombre" autofocus>
                    </div>
                    
                    <div class="form-group">
                    <label for="floatingInputValue">Clave Catastral</label>
                        <input type="text" name="clave" class="form-control" placeholder="Inrese la Edad" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Domicilio</label>
                        <input type="text" name="domicilio" class="form-control" placeholder="Inrese el Domicilio" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">N° Cuenta Prediial</label>
                        <input type="text" name="cuenta" class="form-control" placeholder="Inrese la Cuenta" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Clave Anterior</label>
                        <input type="text" name="anterior" class="form-control" placeholder="Inrese la Clave" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Fecha de Traslado</label>
                        <input type="date" name="fecha" class="form-control" placeholder="Inrese la fecha" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Lugar</label>
                        <input type="text" name="lugar" class="form-control" placeholder="Inrese la Residencia" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Nombre del Terreno</label>
                        <input type="text" name="terreno" class="form-control" placeholder="Inrese el Nombre" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Volumen</label>
                        <input type="text" name="volumen" class="form-control" placeholder="Inrese el Volumen" autofocus>
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Expediente</label>
                        <input type="text" name="expediente" class="form-control" placeholder="Inrese el Expediente" autofocus>
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Hoja o Folio</label>
                        <input type="text" name="hoja" class="form-control" placeholder="Inrese la Hoja o Folio" autofocus>
                    </div>
                    <div class= "d-grid gap-2">
                        <input class="btn btn-success" type="submit" name="save" style="margin-top:5px;"></input>
                    </div>
                </form>
            </div>
        </div>
        <!--Aqui termina el formulario-->
        <div class="col-md-9 offset-md-3 row" style=" position:absolute; top:70px; left:00px; white-space: nowrap">
                <table class="table table-bordered col">
                    <thead> 
                    <tr>
                        <th>Nombre</th> 
                        <th>Clave</th>
                        <th>Domicilio</th>
                        <th>Cuenta Catastral</th>
                        <th>Clave Anteorior</th>
                        <th>Fecha de Traslado</th>
                        <th>Lugar</th>
                        <th>Nombre del Terreno</th>
                        <th>Volumen</th>
                        <th>Expediente</th>
                        <th>Hoja o Folio</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM catastro";
                            $result_r = mysqli_query($conn,$query);

                            while ($row=mysqli_fetch_array($result_r)) {?>
                                <tr>
                                  <td><?php echo $row['nombre']?></td>
                                  <td><?php echo $row['clave']?></td> 
                                  <td><?php echo $row['domicilio']?></td>
                                  <td><?php echo $row['cuenta']?></td>
                                  <td><?php echo $row['anterior']?></td>
                                  <td><?php echo $row['fecha']?></td>
                                  <td><?php echo $row['lugar']?></td>
                                  <td><?php echo $row['terreno']?></td>
                                  <td><?php echo $row['volumen']?></td>
                                  <td><?php echo $row['expediente']?></td>
                                  <td><?php echo $row['folio']?></td>
                                  <td>
                                      <a href="editar.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
                                          <i class = "fas fa-marker"></i>
                                      </a>
                                      <a href="borrar.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                                          <i class="far fa-trash-alt"></i>
                                      </a>
                                  </td>
                                </tr>
                            <?php }?>
                        </tbody>
                </table>
            </div>
    </div>
</div>

<?php 
    include("footer.php");
?>