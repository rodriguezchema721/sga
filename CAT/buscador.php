<style>
  th, td{
    text-align: left;
  }
  .h2g{
    color: blue;
    font-size: 26px;
  }
  .pg{
    line-height: 2px;
  }
</style>

<?php 
include("db.php");

?>


<!-- //resultados buscador -->
<?php 

if (!isset($_POST['buscar'])){$_POST['buscar'] = '';}
if (!isset($_REQUEST["mostrar_todo"])){$_REQUEST["mostrar_todo"] = '';}

if(!empty($_POST))
{

        // resaltamos el resultado
        function resaltar_frase($string, $frase, $taga = '<b>', $tagb = '</b>'){
            return ($string !== '' && $frase !== '')
            ? preg_replace('/('.preg_quote($frase, '/').')/i'.('true' ? 'u' : ''), $taga.'\\1'.$tagb, $string)
            : $string;
             }
    
  
      $aKeyword = explode(" ", $_POST['buscar']);
      $filtro = "WHERE matricula LIKE LOWER('%".$aKeyword[0]."%') OR nombre LIKE LOWER('%".$aKeyword[0]."%')";
      $query ="SELECT * FROM catastro WHERE nombre LIKE LOWER('%".$aKeyword[0]."%') OR clave LIKE LOWER('%".$aKeyword[0]."%')";
  

     for($i = 1; $i < count($aKeyword); $i++) {
        if(!empty($aKeyword[$i])) {
            $query .= " OR matricula LIKE '%" . $aKeyword[$i] . "%' OR nombre LIKE '%" . $aKeyword[$i] . "%'";
        }
      }
     
     $result = $conn->query($query);
     $numero = mysqli_num_rows($result);
     if (!isset($_POST['buscar'])){
     echo "Has buscado la palabra:<b> ". $_POST['buscar']."</b>";
     }

     if( mysqli_num_rows($result) > 0 AND $_POST['buscar'] != '') {
        $row_count=0;
        echo "<br>Resultados encontrados:<b> ".$numero."</b>";
        echo "<br><br><table class='table table-striped'>
        <thead>
        <tr style='background-color:midnightblue; color:#FFFFFF;'>
        <th>ID</th>
        <th>Nombre</th>
        <th>Clave</th> 
        <th>Domicilio</th>
        <th>Cuenta Catastral</th>
        <th>Clave Anterior</th>
        <th>Fecha de Traslado</th>
        <th>Lugar</th>
        <th>Nombre del Terreno</th>
        <th>Volumen</th>
        <th>Expediente</th>
        <th>Hoja o Folio</th>
        </tr>
        </thead>
        ";
        While($row = $result->fetch_assoc()) {   
            $row_count++;   
            echo "<tr><td>".$row_count." </td><td>". resaltar_frase($row['nombre'] ,$_POST['buscar']) . "</td><td>". resaltar_frase($row['clave'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['domicilio'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['cuenta'] ,$_POST['buscar']) ."</td>" . "</td><td>". resaltar_frase($row['anterior'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['fecha'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['lugar'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['terreno'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['volumen'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['expediente'] ,$_POST['buscar']) . "</td>" . "</td><td>". resaltar_frase($row['folio'] ,$_POST['buscar']) .   "</td></tr>";
        }
        echo "</table>";
	
    }
    else {
      //mostramos todos los resultados
      if( $_REQUEST["mostrar_todo"] == 'ok') {
        $row_count=0;
        echo "<br>Resultados encontrados:<b> ".$numero."</b>";
        echo "<br><br><table class='table table-striped'>";
        While($row = $result->fetch_assoc()) {   
            $row_count++;   
            echo "<tr><td>".$row_count." </td><td>". resaltar_frase($row['nombre'] ,$_POST['buscar']) . "</td><td>". resaltar_frase($row['clave'] ,$_POST['buscar']) . "</td></tr>";
        }
        echo "</table>";
	
    }
    }
}
?>