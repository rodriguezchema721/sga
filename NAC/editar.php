<?php
include("db.php");
/*$m = '';
$n= '';
$c = '';
$a= '';
$r = '';
$v= '';
$e = '';
$h= '';*/


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM nacimientos WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $n = $row['nombre'];
    $f = $row['fecha'];
    $p = $row['padre'];
    $m = $row['madre'];
    $r = $row['residencia'];
    $v = $row['volumen'];
    $e = $row['expediente'];
    $h = $row['folio'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $n = $_POST['nombre'];
  $f = $_POST['fecha'];
  $p = $_POST['padre'];
  $m = $_POST['madre'];
  $r = $_POST['residencia'];
  $v = $_POST['volumen'];
  $e = $_POST['expediente'];
  $h = $_POST['hoja'];

  $query = "UPDATE nacimientos set nombre = '$n', fecha = '$f', padre = '$p', madre = '$m', residencia = '$r', volumen = '$v', expediente = '$e', folio = '$h' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Modificado Satisfactoriamente';
  $_SESSION['message_type'] = 'warning';
  header('location: Nac.php');
}

?>
<?php include('header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="editar.php?id=<?php echo $_GET['id']; ?>" method="POST">
      <div class="form-group">
                     <label for="floatingInputValue"><b>Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Inrese el Nombre" autofocus value="<?php echo $row['nombre']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Fecha de Nacimiento</label>
                        <input type="date" name="fecha" class="form-control" placeholder="Inrese la fecha" autofocus value="<?php echo $row['fecha']?>">
                    </div>
                    
                    <div class="form-group">
                    <label for="floatingInputValue">Nombre del Padre</label>
                        <input type="text" name="padre" class="form-control" placeholder="Inrese el Nombre" autofocus value="<?php echo $row['padre']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Nombre de la Madre</label>
                        <input type="text" name="madre" class="form-control" placeholder="Inrese el Nombre" autofocus value="<?php echo $row['madre']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Residencia</label>
                        <input type="text" name="residencia" class="form-control" placeholder="Inrese la Residencia" autofocus value="<?php echo $row['residencia']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Volumen</label>
                        <input type="text" name="volumen" class="form-control" placeholder="Inrese el Volumen" autofocus value="<?php echo $row['volumen']?>">
                    </div>
                                        
                    <div class="form-group">
                    <label for="floatingInputValue">Expediente</label>
                        <input type="text" name="expediente" class="form-control" placeholder="Inrese el Expediente" autofocus value="<?php echo $row['expediente']?>">
                    </div>

                    <div class="form-group">
                    <label for="floatingInputValue">Hoja o Folio</label>
                        <input type="text" name="hoja" class="form-control" placeholder="Inrese la Hoja o Folio" autofocus value="<?php echo $row['folio']?>">
                    </div>
                    <div class= "d-grid gap-2">
                        <input class="btn btn-success" type="submit" name="update" style="margin-top:5px;"></input>
                    </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>