<?php

define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'validar');
 
/* Attempt to connect to MySQL database */
$consulta = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Check connection
if($consulta === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
?>